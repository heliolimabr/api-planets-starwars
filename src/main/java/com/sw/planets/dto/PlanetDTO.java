package com.sw.planets.dto;

import com.sw.planets.services.validation.PlanetInsert;
import javax.validation.constraints.NotEmpty;

/**
 *
 * @author Helio
 */
@PlanetInsert
public class PlanetDTO {

    @NotEmpty(message = "Preenchimento Obrigatório")
    private String nome;
    @NotEmpty(message = "Preenchimento Obrigatório")
    private String clima;
    @NotEmpty(message = "Preenchimento Obrigatório")
    private String terreno;

    public PlanetDTO() {
    }

    public PlanetDTO(String nome, String clima, String terreno) {
        this.nome = nome;
        this.clima = clima;
        this.terreno = terreno;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getClima() {
        return clima;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }

    public String getTerreno() {
        return terreno;
    }

    public void setTerreno(String terreno) {
        this.terreno = terreno;
    }
    
    
}
