package com.sw.planets.repositories;

import com.sw.planets.business.Planet;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Helio
 */
@Repository
public interface PlanetRepository extends MongoRepository<Planet, String>{

    @Transactional(readOnly = true)
    Planet findByNome(String nome);
}
