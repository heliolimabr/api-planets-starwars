package com.sw.planets.resources;

import com.sw.planets.business.Planet;
import com.sw.planets.dto.PlanetDTO;
import com.sw.planets.services.PlanetService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.net.URI;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author Helio
 */
@Api(value = "Planets", tags = { "Planets" })
@RestController
@RequestMapping(value = "/planets")
public class PlanetResource {

    @Autowired
    private PlanetService service;
    
    @ApiOperation(value="Busca por id")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Planet> findById(@PathVariable String id){
        Planet obj = service.find(id);
        return ResponseEntity.ok(obj);
    }
    
    @ApiOperation(value="Busca por nome")
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<Planet> findByNome(@RequestParam(value = "nome") String nome){
        Planet obj = service.findByNome(nome);
        return ResponseEntity.ok(obj);
    }
    
    @ApiOperation(
            value="Inserir um planeta", 
            notes = "Ao inserir corretamente, a API faz uma busca para "
                    + "recuperar a quantidade de aparições em filmes.")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> insert(@Valid @RequestBody PlanetDTO planeta){
        Planet obj = service.fromDTO(planeta);
        obj = service.insert(obj);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(obj.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }
    
    @ApiOperation(value="Listar todos os planetas")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Planet>> findAll(){
        List<Planet> list = service.findAll();
        return ResponseEntity.ok(list);
    }
    
    @ApiOperation(value="Exclui um planeta")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable String id){
        service.delete(id);
        return ResponseEntity.noContent().build();
    }
}
