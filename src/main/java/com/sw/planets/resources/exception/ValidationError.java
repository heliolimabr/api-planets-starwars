package com.sw.planets.resources.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe utilizada para auxiliar na exibição de erros de validação
 * exibindo em lista os campos afetados.
 *
 * @author Helio
 */
public class ValidationError  extends StandardError {

    private List<FieldMessage> errors = new ArrayList<FieldMessage>();

    public ValidationError(Long timeStamp, Integer status, String error, String message, String path) {
        super(timeStamp, status, error, message, path);
    }
    
    public List<FieldMessage> getErrors() {
        return errors;
    }

    public void addError(String fieldName, String message) {
        this.errors.add(new FieldMessage(fieldName, message));
    }
}
