package com.sw.planets.services.validation;

import com.sw.planets.business.Planet;
import com.sw.planets.dto.PlanetDTO;
import com.sw.planets.repositories.PlanetRepository;
import com.sw.planets.resources.exception.FieldMessage;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Helio
 */
public class PlanetInsertValidator implements ConstraintValidator<PlanetInsert, PlanetDTO>{

    @Autowired
    private PlanetRepository repo;
    
    @Override
    public boolean isValid(PlanetDTO obj, ConstraintValidatorContext context) {
        
        List<FieldMessage> list = new ArrayList<>();
        
        Planet planet = repo.findByNome(obj.getNome());
        if(planet != null){
            list.add(new FieldMessage("Nome", "Nome existente!"));
        }
        
        list.forEach((e) -> {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName())
                    .addConstraintViolation();
        });
        return list.isEmpty();
    }

}
