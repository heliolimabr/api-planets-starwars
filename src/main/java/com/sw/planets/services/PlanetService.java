package com.sw.planets.services;

import com.sw.planets.business.Planet;
import com.sw.planets.dto.PlanetDTO;
import com.sw.planets.repositories.PlanetRepository;
import com.sw.planets.services.exceptions.ObjectNotFoundException;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Helio
 */
@Service
public class PlanetService {

    @Autowired
    private PlanetRepository repo;
    
    @Autowired
    private SwapiService swapiService;
    
    @Transactional
    public Planet insert(Planet obj){
        obj.setAparicoes(swapiService.getPlanetAparicoes(obj.getNome()));
        obj = repo.save(obj);
        return obj;
    }
    
    public Planet find(String id){
        Optional<Planet> obj = repo.findById(id);
        
        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não "
                + "encontrado! Id: " + id + ", tipo: " + Planet.class.getName()));
    }
    
    public Planet findByNome(String nome){
        Planet obj = repo.findByNome(nome);
        
        if(obj == null){
            throw new ObjectNotFoundException("Objeto não "
                + "encontrado! Nome: " + nome + ", "
                + "tipo: " + Planet.class.getName());
        }
        return obj;
    }
    
    public Planet update(Planet obj){
        Planet newObj = find(obj.getId());
        updateData(newObj, obj);
        return repo.save(newObj);
    }
    
    public void delete(String id){
        //chama o método find(), pois ele já checa se o objeto existe
        find(id);
        repo.deleteById(id);
    }
    
    public List<Planet> findAll(){
        return repo.findAll();
    }
    
    public Planet fromDTO(PlanetDTO objDTO){
        return new Planet(objDTO.getNome(), objDTO.getClima(), objDTO.getTerreno());
    }
    
    public void updateData(Planet newObj, Planet obj){
        newObj.setNome(obj.getNome());
        newObj.setClima(obj.getClima());
        newObj.setTerreno(obj.getTerreno());
    }
    
    
    
}
