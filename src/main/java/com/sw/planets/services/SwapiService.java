package com.sw.planets.services;

import com.sw.planets.business.swapi.SwapiPlanet;
import com.sw.planets.business.swapi.SwapiResult;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Helio
 */
@Service
public class SwapiService {

    @Value("${planets.swapi.uri}")
    private String uriSwapi;

    /**
     * O método faz busca na API externa pelo nome do planeta cadastrado
     * 
     * @param name
     * @return int 
     */
    public int getPlanetAparicoes(String name) {
        int aparicoes = 0;
        //tenta recuperar dados da api externa, se houver algum erro o retorno será 0
        try {
            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .build();
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);
            RestTemplate restTemplate = new RestTemplate(requestFactory);
            SwapiResult result = restTemplate.getForObject(uriSwapi + "planets/?search=" + name, SwapiResult.class);
            if (result != null && result.getCount() > 0) {
                SwapiPlanet planet = result.getResults()[0];
                aparicoes = planet.getFilms().length;
            }
        } catch (Exception e) {
            aparicoes = 0;
        }
        return aparicoes;
    }
}
