package com.sw.planets.business;

import javax.validation.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Helio
 */
@Document(collection = "Planets")
public class Planet {
    
    @Id
    private String id;
    
    @Indexed
    @NotEmpty(message = "Preenchimento Obrigatório")
    private String nome;
    @NotEmpty(message = "Preenchimento Obrigatório")
    private String clima;
    @NotEmpty(message = "Preenchimento Obrigatório")
    private String terreno;
    private int aparicoes;

    public Planet() {
    }

    public Planet(String nome, String clima, String terreno) {
        this.nome = nome;
        this.clima = clima;
        this.terreno = terreno;
        this.aparicoes = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getClima() {
        return clima;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }

    public String getTerreno() {
        return terreno;
    }

    public void setTerreno(String terreno) {
        this.terreno = terreno;
    }

    public int getAparicoes() {
        return aparicoes;
    }

    public void setAparicoes(int aparicoes) {
        this.aparicoes = aparicoes;
    }
    
    
}
