package com.sw.planets.business.swapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Helio
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SwapiPlanet {

    private String name;
    private String[] films;

    public SwapiPlanet() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getFilms() {
        return films;
    }

    public void setFilms(String[] films) {
        this.films = films;
    }
    
    
}
