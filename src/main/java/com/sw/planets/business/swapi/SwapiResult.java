package com.sw.planets.business.swapi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 *
 * @author Helio
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SwapiResult {

    private int count;
    private SwapiPlanet[] results;

    public SwapiResult() {
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public SwapiPlanet[] getResults() {
        return results;
    }

    public void setResults(SwapiPlanet[] results) {
        this.results = results;
    }
    
    
}
