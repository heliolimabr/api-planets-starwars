package com.sw.planets.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Header;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * @author Helio
 */
@EnableSwagger2
@Configuration
public class SwaggerConfig {

    private final ResponseMessage m201 = messageHeader();
    private final ResponseMessage m204put = simpleMessage(204, "Atualização ok");
    private final ResponseMessage m204del = simpleMessage(204, "Exclusão ok");
    private final ResponseMessage m404 = simpleMessage(404, "Não encontrado");
    private final ResponseMessage m422 = simpleMessage(422, "Erro de validação");
    private final ResponseMessage m500 = simpleMessage(500, "Erro inesperado");

    @Bean
    public Docket detalheApi() {

        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .globalResponseMessage(RequestMethod.GET, Arrays.asList(m404, m500))
                .globalResponseMessage(RequestMethod.POST, Arrays.asList(m201, m422, m500))
                .globalResponseMessage(RequestMethod.PUT, Arrays.asList(m204put, m404, m422, m500))
                .globalResponseMessage(RequestMethod.DELETE, Arrays.asList(m204del, m404, m500))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sw.planets.resources"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(this.informacoesApi().build());

        return docket;
    }

    private ApiInfoBuilder informacoesApi() {

        ApiInfoBuilder apiInfoBuilder = new ApiInfoBuilder();

        apiInfoBuilder.title("API Star Wars Planets");
        apiInfoBuilder.description("Api para dados de planetas de Star Wars.");
        apiInfoBuilder.version("1.0");
        apiInfoBuilder.contact(new Contact(
                "Helio Lima",
                "",
                "helio.augustolima@gmail.com"));

        return apiInfoBuilder;

    }

    private ResponseMessage simpleMessage(int code, String msg) {
        return new ResponseMessageBuilder().code(code).message(msg).build();
    }

    private ResponseMessage messageHeader() {
        Map<String, Header> map = new HashMap<>();
        map.put("location", new Header("location", "URI do novo registro", new ModelRef("string")));
        return new ResponseMessageBuilder()
                .code(201)
                .message("Registro criado")
                .headersWithDescription(map)
                .build();
    }

}
