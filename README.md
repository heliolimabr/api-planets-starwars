# API Star Wars Planets
API para dados de planetas de Star Wars.

## O que foi utilizado
Java - 1.8.x

Maven - 3.x.x

Spring Boot - 2.1.x

MongoDB - 4.x.x

Swagger - 2.x

## Passos para executar

### 1. Criar banco de dados MongoDB

A configuração padrão aponta para o banco de dados StarWars. Se o usuário tiver permissão de criar banco, ele será criado automaticamente.

### 2. Alterar configuração de banco de dados

abrir src/main/resources/application.properties

alterar spring.data.mongodb.uri de acordo com sua instalação do MongoDB

Ex.: mongodb://localhost:27017/StarWars para usuário padrão do servidor ou mongodb://username:password@localhost:27017/StarWars

### 3. Execute o projeto

```bash
mvn spring-boot:run
```

O projeto irá rodar no endereço http://localhost:8080.


## Informações da API

Endpoints:

GET /planets

POST /planets

GET /planets/{id}

GET /planets/search

DELETE /planets/{id}

#### Para acessar a documentação acesse com o projeto rodando: http://localhost:8080/swagger-ui.html

